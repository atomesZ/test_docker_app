import main
import socket

def test_index():

    c = main.app.test_client()
    ret = c.get("/")

    ## getting the hostname by socket.gethostname() method
    hostname = socket.gethostname()

    ## getting the IP address using socket.gethostbyname() method
    ip_address = socket.gethostbyname(hostname)


    assert ret.data.decode('utf-8') == f"<h1>Main page</h1><p>Hostname: {hostname}</p><p>IP Address: {ip_address}</p>"
