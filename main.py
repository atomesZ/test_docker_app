from flask import Flask
import socket

app = Flask(__name__)

@app.route("/")
def index():
    ## getting the hostname by socket.gethostname() method
    hostname = socket.gethostname()

    ## getting the IP address using socket.gethostbyname() method
    ip_address = socket.gethostbyname(hostname)


    return f"<h1>Main page</h1><p>Hostname: {hostname}</p><p>IP Address: {ip_address}</p>"
