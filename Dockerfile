FROM python:3.8.5

WORKDIR /usr/src/app

ENV FLASK_APP main:app
ENV FLASK_RUN_HOST 0.0.0.0

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "flask", "run", "-p 8888" ]
